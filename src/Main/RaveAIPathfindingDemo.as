package Main
{
	import GameObjects.Terrain;
	import GameObjects.Triangle;
	
	import Math3D.PerlinNoiseTerrainGenerator;
	
	import Pathfinding.AStarHelper;
	import Pathfinding.PathNode;
	
	import RaveRenderingUtils2.EngineHelpers.EngineAgnosticInitHelper;
	import RaveRenderingUtils2.EngineHelpers.EngineAgnosticRenderingHelper;
	import RaveRenderingUtils2.EngineHelpers.EngineAgnosticStateHolder;
	import RaveRenderingUtils2.EngineHelpers.MinkoInitHelper;
	import RaveRenderingUtils2.Generators.CubeGenerator;
	import RaveRenderingUtils2.Generators.HeightmapTerrainGenerator;
	import RaveRenderingUtils2.Generators.PathGenerator;
	import RaveRenderingUtils2.Generators.TerrainGenerator;
	import RaveRenderingUtils2.Generators.TriangleGenerator;
	import RaveRenderingUtils2.Main.MinkoApplication;
	import RaveRenderingUtils2.Managers.PathManager;
	import RaveRenderingUtils2.Primitives.PointsMesh;
	import RaveRenderingUtils2.Primitives.TerrainMesh;
	import RaveRenderingUtils2.Primitives.TriangleMesh;
	import RaveRenderingUtils2.Shaders.NormalsShader;
	import RaveRenderingUtils2.Utils.MinkoTerrainSampler;
	
	import Tools.LogBuffer;
	
	import aerys.minko.Minko;
	import aerys.minko.render.effect.Effect;
	import aerys.minko.scene.node.mesh.Mesh;
	import aerys.minko.type.enum.Blending;
	import aerys.minko.type.loader.TextureLoader;
	import aerys.minko.type.log.DebugLevel;
	import aerys.minko.type.math.Matrix4x4;
	import aerys.minko.type.math.Vector4;
	
	import com.demonsters.debugger.MonsterDebugger;
	
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.geom.Vector3D;
	import flash.profiler.showRedrawRegions;
	
	[SWF(frameRate="60", width="1600", height="1200")]	
	public class RaveAIPathfindingDemo extends MinkoApplication
	{
		protected var _matrix	: Matrix4x4	= new Matrix4x4();
		
		[Embed(source = "..\\..\\..\\assets\\grass.jpg")]
		private static const ASSET_TEXTURE	: Class;
		
		[Embed(source = "..\\..\\..\\assets\\terrain3.png")]
		private static const ASSET_TERRAIN	: Class;
		
		private var _showPoints : Boolean = true;
		private var _terrainYTranslation : Number = -10;
		
		private static const NUM_POINTS	: uint	= 100000;
		
		private function RaveInit() : void
		{			
			EngineAgnosticStateHolder.MainSprite = this;
			EngineAgnosticStateHolder.AiContext = new RaveContext(new EmbeddedSettingsText().toString(), ASSET_TERRAIN/*, new EmbeddedFsmText().toString()*/);
		}
		
		override protected function initializeScene() : void
		{		
			// Start the MonsterDebugger
			MonsterDebugger.initialize(this);
			MonsterDebugger.trace(this, "Monster debugger started.");
			
			Minko.debugLevel = DebugLevel.SHADER_AGAL;
			
			RaveInit();
			EngineAgnosticInitHelper.InitHUD();
			EngineAgnosticInitHelper.InitUI();
			
			scene.properties.setProperties({
				lightEnabled		: true,
				lightDiffuseColor	: 0xffffff,
				lightDiffuse		: 0.8,
				lightAmbientColor	: 0xffffff,
				lightAmbient		: 0.5
			});
			
			var size : int = 128;
			
			MinkoTerrainSampler.Width = size;
			MinkoTerrainSampler.Height = size;
						
			MinkoTerrainSampler.Step = 1;
			var terrainInstance : Terrain = MinkoTerrainSampler.sample(new Bitmap(new ASSET_TERRAIN().bitmapData)); 
			
			MinkoTerrainSampler.Step = 4;
			var partialTerrainInstance : Terrain = MinkoTerrainSampler.sample(new Bitmap(new ASSET_TERRAIN().bitmapData));
			
			EngineAgnosticStateHolder.AiContext.FullTerrain = terrainInstance;
			EngineAgnosticStateHolder.AiContext.PartialTerrain = partialTerrainInstance;
			
			EngineAgnosticStateHolder.AiContext.InitializePhase1();			
			EngineAgnosticStateHolder.AiContext.InitializePhase2();
			
			EngineAgnosticStateHolder.AiContext.AStarHelperInstance = EngineAgnosticStateHolder.AiContext.GetNewAStarInstance();
			
			cameraController.zoom(20);
			
			var terrainMesh		: Mesh = HeightmapTerrainGenerator.generate(size, size, new Bitmap(new ASSET_TERRAIN().bitmapData), ASSET_TEXTURE); 
			
			if (_showPoints)
			{
				MinkoInitHelper.ShowWaypoints(scene, partialTerrainInstance);
				/*MinkoInitHelper.ShowPathNodes(scene, EngineAgnosticStateHolder.AiContext);
				MinkoInitHelper.ShowConnections(scene, EngineAgnosticStateHolder.AiContext);*/
			}
			
			scene.addChild(terrainMesh);
			
			scene.properties.setProperty(
				'lightPosition',
				new Vector3D(0, 0, 0)
			);
		}
		
		override protected function enterFrameHandler(event : Event) : void
		{
			_matrix.appendRotation(.05, Vector4.Y_AXIS);
			scene.properties.setProperty(
				'lightDirection',
				_matrix.transformVector(Vector4.ONE)
			);
			
			EngineAgnosticRenderingHelper.DisplayFPS();
			EngineAgnosticRenderingHelper.ActualizeUI();
			
			var pn1 : PathNode, pn2 : PathNode;
			
			pn1 = EngineAgnosticStateHolder.AiContext.PathNodeGeneratorInstance.GetRandomPathNode();
			pn2 = EngineAgnosticStateHolder.AiContext.PathNodeGeneratorInstance.GetRandomPathNodeFarFrom(pn1);
			
			var path : Vector.<PathNode> = EngineAgnosticStateHolder.AiContext.AStarHelperInstance.Calculate(pn1, pn2);
			
			if (path != null)
			{
				PathManager.PathsToAdd.push(path);
			}
			else
			{
				LogBuffer.Trace("Path not found");	
			}
				
			PathGenerator.Display3DPath(scene);
			
			super.enterFrameHandler(event);
		}
	}
}